package org.task.oven.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.task.oven.entities.Oven;
import org.task.oven.enums.Power;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class OvenMapperTest {
    private static final Power TEST_POWER = Power.ON;
    private static final int TEST_TEMPERATURE = 100;
    private static final int TEST_TIMER = 0;

    @Autowired
    private OvenMapper ovenMapper;

    @Test
    public void get() {
        Oven oven = ovenMapper.get();

        assertThat(oven.getPower(), is(TEST_POWER));
        assertThat(oven.getTemperature(), is(TEST_TEMPERATURE));
        assertThat(oven.getTimer(), is(TEST_TIMER));
    }

    @Test
    public void update() {
        Power newPower = Power.OFF;
        int newTemperature = 150;
        int newTimer = 60;
        Oven oven = new Oven(newPower, newTemperature, newTimer);

        boolean result = ovenMapper.update(oven);
        assertThat(result, is(true));

        Oven updatedOven = ovenMapper.get();
        assertThat(updatedOven.getPower(), is(newPower));
        assertThat(updatedOven.getTemperature(), is(newTemperature));
        assertThat(updatedOven.getTimer(), is(newTimer));
    }

    @Test
    public void updatePower() {
        Power newPower = Power.OFF;

        boolean result = ovenMapper.updatePower(newPower);
        assertThat(result, is(true));

        Oven oven = ovenMapper.get();
        assertThat(oven.getPower(), is(newPower));
        assertThat(oven.getTemperature(), is(TEST_TEMPERATURE));
        assertThat(oven.getTimer(), is(TEST_TIMER));
    }

    @Test
    public void updateTemperature() {
        int newTemperature = 150;

        boolean result = ovenMapper.updateTemperature(newTemperature);
        assertThat(result, is(true));

        Oven oven = ovenMapper.get();
        assertThat(oven.getPower(), is(TEST_POWER));
        assertThat(oven.getTemperature(), is(newTemperature));
        assertThat(oven.getTimer(), is(TEST_TIMER));
    }

    @Test
    public void updateTimer() {
        int newTimer = 33;

        boolean result = ovenMapper.updateTimer(newTimer);
        assertThat(result, is(true));

        Oven oven = ovenMapper.get();
        assertThat(oven.getPower(), is(TEST_POWER));
        assertThat(oven.getTemperature(), is(TEST_TEMPERATURE));
        assertThat(oven.getTimer(), is(newTimer));
    }

}
