package org.task.oven.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.task.oven.entities.Oven;
import org.task.oven.enums.Power;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class OvenServiceTest {
    @Autowired
    private OvenService ovenService;

    @Test
    public void get() {
        Oven state = ovenService.getState();

        assertEquals(initialOven(), state);
    }

    @Test
    public void successfullyUpdateOven() {
        Power newPower = Power.OFF;
        int newTemperature = 150;
        int newTimer = 60;

        Oven oven = ovenService.getState();
        oven.setPower(newPower);
        oven.setTemperature(newTemperature);
        oven.setTimer(newTimer);

        Oven updated = ovenService.update(oven);

        assertEquals(oven, updated);
        assertThat(updated.getPower(), is(newPower));
        assertThat(updated.getTemperature(), is(newTemperature));
        assertThat(updated.getTimer(), is(newTimer));
    }

    @Test(expected = IllegalArgumentException.class)
    public void failedToUpdate() {
        ovenService.update(null);
    }

    @Test
    public void successfullyUpdatePower() {
        ovenService.turnOff();

        Oven state = ovenService.getState();
        assertThat(state.getPower(), is(Power.OFF));

        ovenService.turnOn();
        ovenService.turnOn();

        state = ovenService.getState();
        assertThat(state.getPower(), is(Power.ON));
    }

    @Test
    public void successfullyUpdateTemperature() {
        int newTemperature = 150;
        ovenService.updateTemperature(newTemperature);
        assertThat(ovenService.getState().getTemperature(), is(newTemperature));
    }

    @Test(expected = IllegalArgumentException.class)
    public void failedToUpdateTemperature() {
        ovenService.updateTemperature(-10);
    }

    @Test
    public void successfullyUpdateTimer() {
        int newTimer = 30;
        ovenService.updateTimer(newTimer);
        assertThat(ovenService.getState().getTimer(), is(newTimer));
    }

    @Test(expected = IllegalArgumentException.class)
    public void failedToUpdateTimer() {
        ovenService.updateTimer(10000);
    }

    private Oven initialOven() {
        return new Oven(Power.ON, 100, 0);
    }
}
