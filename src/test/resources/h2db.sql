DROP ALL objects;

CREATE TABLE IF NOT EXISTS oven(
  power VARCHAR(3) NOT NULL check(power in ('ON', 'OFF')),
  temperature INT UNSIGNED NOT NULL,
  timer INT UNSIGNED NULL)
;