package org.task.oven.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.task.oven.dao.OvenMapper;
import org.task.oven.entities.Oven;
import org.task.oven.enums.Power;
import org.task.oven.utilities.ValidationUtility;

@Service
public class OvenService {

    private OvenMapper ovenMapper;

    @Autowired
    public OvenService(OvenMapper ovenMapper) {
        this.ovenMapper = ovenMapper;
    }


    public Oven getState() {
        return ovenMapper.get();
    }

    public Oven update(Oven fromReq) {
        if (ValidationUtility.isEmpty(fromReq)) {
            throw new IllegalArgumentException("Parameters are not specified");
        }
        if (!ValidationUtility.isValidTemperature(fromReq.getTemperature())) {
            throw new IllegalArgumentException("Temperature must be between 60 and 200 degrees celsius");
        }
        if (!ValidationUtility.isValidTimer(fromReq.getTimer())) {
            throw new IllegalArgumentException("Timer must be not more than 180 minutes");
        }

        if (ovenMapper.update(fromReq))
            return fromReq;
        else
            throw new RuntimeException("Update failed");
    }

    public void turnOn() {
        Oven state = getState();
        if (!state.getPower().equals(Power.ON)) {
            ovenMapper.updatePower(Power.ON);
        }
    }

    public void turnOff() {
        Oven state = getState();
        if (!state.getPower().equals(Power.OFF)) {
            ovenMapper.updatePower(Power.OFF);
        }
    }

    public void updateTemperature(int newTemperature) {
        if (!ValidationUtility.isValidTemperature(newTemperature)) {
            throw new IllegalArgumentException("Temperature must be between 60 and 200 degrees celsius");
        }

        ovenMapper.updateTemperature(newTemperature);
    }

    public void updateTimer(int newTimer) {
        if (!ValidationUtility.isValidTimer(newTimer)) {
            throw new IllegalArgumentException("Timer must be not more than 180 minutes");
        }

        ovenMapper.updateTimer(newTimer);
    }
}
