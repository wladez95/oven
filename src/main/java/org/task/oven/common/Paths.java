package org.task.oven.common;

public final class Paths {
    public static final String OVEN = "/oven";
    public static final String ON = "/on";
    public static final String OFF = "/off";
    public static final String TEMPERATURE = "/temperature";
    public static final String TIMER = "/timer";
}
