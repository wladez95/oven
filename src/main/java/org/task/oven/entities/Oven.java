package org.task.oven.entities;

import org.task.oven.enums.Power;

import java.util.Objects;

public class Oven {
    private Power power;
    private int temperature;
    private int timer;

    public Oven() {
    }

    public Oven(Power power, int temperature, int timer) {
        this.power = power;
        this.temperature = temperature;
        this.timer = timer;
    }

    public Power getPower() {
        return power;
    }

    public void setPower(Power power) {
        this.power = power;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Oven oven = (Oven) o;
        return power == oven.power &&
                temperature == oven.temperature &&
                timer == oven.timer;
    }

    @Override
    public int hashCode() {
        return Objects.hash(power, temperature, timer);
    }

    @Override
    public String toString() {
        return "Oven{" +
                "power=" + power +
                ", temperature=" + temperature +
                ", timer=" + timer +
                '}';
    }
}
