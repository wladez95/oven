package org.task.oven.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.task.oven.common.Paths;
import org.task.oven.entities.Oven;
import org.task.oven.services.OvenService;

@RestController
@RequestMapping(Paths.OVEN)
public class OvenController {

    private OvenService ovenService;

    @Autowired
    public OvenController(OvenService ovenService) {
        this.ovenService = ovenService;
    }

    @GetMapping
    public Oven getState() {
        return ovenService.getState();
    }

    @PutMapping
    public Oven update(@RequestBody Oven oven) {
        return ovenService.update(oven);
    }

    @PutMapping(Paths.ON)
    public void turnOn() {
        ovenService.turnOn();
    }

    @PutMapping(Paths.OFF)
    public void turnOff() {
        ovenService.turnOff();
    }

    @GetMapping(Paths.TEMPERATURE)
    public int getTemperature() {
        return ovenService.getState().getTemperature();
    }

    @PutMapping(Paths.TEMPERATURE)
    public void updateTemperature(@RequestBody int temperature) {
        ovenService.updateTemperature(temperature);
    }

    @GetMapping(Paths.TIMER)
    public int getTimer() {
        return ovenService.getState().getTimer();
    }

    @PutMapping(Paths.TIMER)
    public void updateTimer(@RequestBody int timer) {
        ovenService.updateTimer(timer);
    }
}
