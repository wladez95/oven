package org.task.oven.utilities;

public class ValidationUtility {

    public static boolean isEmpty(final Object object) {
        return object == null;
    }

    public static boolean isValidTimer(int timer) {
        return timer > 0 && timer < 180;
    }

    public static boolean isValidTemperature(int temperature) {
        return temperature > 60 && temperature < 200;
    }
}
