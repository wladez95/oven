package org.task.oven.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;
import org.task.oven.entities.Oven;
import org.task.oven.enums.Power;

@Mapper
@Component
public interface OvenMapper {

    @Select("SELECT power, temperature, timer FROM oven")
    Oven get();

    @Update("UPDATE oven SET power = #{power}, temperature = #{temperature}, timer = #{timer}")
    boolean update(Oven oven);

    @Update("UPDATE oven SET power = #{power}")
    boolean updatePower(@Param("power") Power power);

    @Update("UPDATE oven SET temperature = #{temperature}")
    boolean updateTemperature(@Param("temperature") int temperature);

    @Update("UPDATE oven SET timer = #{timer}")
    boolean updateTimer(@Param("timer") int timer);
}
