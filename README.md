# oven

| HTTP method | URL | Description |
| :-----------: | :---:| :-----------: |
| GET | /oven | returns state of oven |
| PUT | /oven | changes state of oven |
| PUT | /oven/on | turns on the power |
| PUT | /oven/off | turns off the power |
| GET | /oven/temperature | returns temperature in oven |
| PUT | /oven/temperature | changes temperature |
| GET | /oven/timer | returns timer value |
| PUT | /oven/timer | changes timer value |

In all PUT requests **Content-Type** must be **application/json**
